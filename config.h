/*
Copyright 2018 Mattia Dal Ben <matthewdibi@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

// wiring of each half
#undef MATRIX_ROW_PINS
#define MATRIX_ROW_PINS { A7, B3, A2, B1, B0 }  // B5 B4 B2 has some problem with right side
#undef MATRIX_COL_PINS
#define MATRIX_COL_PINS { A1, A0, B8, B13, B14, B15, B9 }

#undef RGB_DI_PIN

#define SPLIT_HAND_PIN      C13  // high = left, low = right

#define SPLIT_TRANSACTION_IDS_KB WATCHDOG_SYNC

#undef SERIAL_USART_PIN_SWAP    // Swap TX and RX pins if keyboard is master halve.
#define SERIAL_USART_FULL_DUPLEX   // Enable full duplex operation mode.
#undef  SOFT_SERIAL_PIN
#undef SERIAL_USART_TX_PIN
#undef SERIAL_USART_RX_PIN
#define SERIAL_USART_TX_PIN A9      // USART TX pin
#define SERIAL_USART_RX_PIN A10     // USART RX pin


#define SELECT_SOFT_SERIAL_SPEED 1 // or 0, 2, 3, 4, 5
                                   //  0: 460800 baud
                                   //  1: 230400 baud (default)
                                   //  2: 115200 baud
                                   //  3: 57600 baud
                                   //  4: 38400 baud
                                   //  5: 19200 baud
#define SERIAL_USART_DRIVER SD1    // USART driver of TX and RX pin. default: SD1
#define SERIAL_USART_TX_PAL_MODE 7 // Pin "alternate function", see the respective datasheet for the appropriate values for your MCU. default: 7
#define SERIAL_USART_RX_PAL_MODE 7 // Pin "alternate function", see the respective datasheet for the appropriate values for your MCU. default: 7
#define SERIAL_USART_TIMEOUT 20    // USART driver timeout. default 20


#undef DEBOUNCE
#define DEBOUNCE 8


#define TAPPING_TERM 200
#define TAPPING_TERM_PER_KEY

/* Word cap enabled when both r and l shift are pressed  */
#define BOTH_SHIFTS_TURNS_ON_CAPS_WORD
/* This help with camel case  */
#define CAPS_WORD_INVERT_ON_SHIFT



#ifdef OLED_ENABLE
    #define OLED_DISPLAY_128X64
    #define SPLIT_OLED_ENABLE
    #define OLED_DISPLAY_ADDRESS     0x3C //0x78
    #define OLED_BRIGHTNESS 128
    #define I2C_DRIVER        I2CD3
    #define I2C1_SCL_PIN     A8  //B10 I2C2 ok // B6 B8 I2C1 has some problems in some aliexpress f401 try to use weact f411 genuine( only boards with bootloaders problem?)
    #define I2C1_SDA_PIN     B4   //B3  I2C2 ok // B7 B9 I2C1 has some problems in some aliexpress f401 try to use weact f411 genuine( only boards with bootloaders problem?)
    #define I2C1_SCL_PAL_MODE 4
    #define I2C1_SDA_PAL_MODE 9
    #define I2C1_CLOCK_SPEED  400000
    #define I2C1_DUTY_CYCLE FAST_DUTY_CYCLE_2
#endif

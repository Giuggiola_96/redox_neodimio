// Copyright 2018-2022 Nick Brassel (@tzarc)
// SPDX-License-Identifier: GPL-2.0-or-later
#pragma once
#include_next <mcuconf.h>

// Used for audio


// Used for backlight



// Used for split comms
#undef STM32_SERIAL_USE_USART1
#define STM32_SERIAL_USE_USART1 TRUE
#undef STM32_I2C_USE_I2C1
#undef STM32_I2C_USE_I2C3
#define STM32_I2C_USE_I2C3 TRUE

// #define STM32_I2C_BUSY_TIMEOUT		50U
// #define STM32_I2C_I2C1_IRQ_PRIORITY		10
// #define STM32_I2C_USE_DMA		TRUE
// #define STM32_I2C_I2C1_DMA_PRIORITY	1


// #define I2C1_OPMODE	OPMODE_I2C
